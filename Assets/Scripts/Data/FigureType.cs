﻿using System;

[Serializable]
public enum FigureType
{
    SPHERE = 0,
    CAPSULE = 1,
    CYLINDER = 2,
    CUBE = 3,
    PLANE = 4,
    QUAD = 5

}
