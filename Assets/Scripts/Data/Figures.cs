﻿using System;
using UnityEngine;

[Serializable]
public class Figures
{
    public string type;
    public float posX;
    public float posY;
    public float edgeLength;
    public float radius;
    public Vector3Ser[] vertices;
}

[Serializable]
public class Vector3Ser
{
    public float x, y, z;
}