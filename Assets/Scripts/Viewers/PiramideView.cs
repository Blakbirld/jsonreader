﻿using UnityEngine;

[RequireComponent(typeof(MeshRenderer))]
[RequireComponent(typeof(MeshFilter))]
public class PiramideView : MonoBehaviour, IViewer
{
	private MeshFilter meshFilter;
	private Mesh mesh;
	
	private void Awake()
	{
        meshFilter = gameObject.GetComponent<MeshFilter>();
		var meshRenderer = gameObject.GetComponent<MeshRenderer>();
		mesh = meshFilter.mesh;
		
		var material = Resources.Load<Material>("Prefabs/Material");
		meshRenderer.material = material;
	
		mesh.Clear();

		SetDefaultVertices();

		mesh.uv = new [] { new Vector2(0, 0), new Vector2(0, 1), new Vector2(1, 1), new Vector2(1, 0) };
		mesh.triangles =  new [] { 0, 1, 2, 0, 1, 3, 1, 2, 3, 0, 3, 2 };
	}

	private void SetDefaultVertices()
	{
		mesh.vertices = new []
		{
			new Vector3(0.0f, 0.0f, 0.0f), 
			new Vector3(0.0f, 1.0f, 0.0f), 
			new Vector3(1.0f, 1.0f, 0.0f), 
			new Vector3(0.32f, 0.68f, -1.0f),
		};
	}

    public void SetupViewer(Figures _figure)
    {
        Vector3[] vectors = new Vector3[_figure.vertices.Length];
        for (int i = 0; i < vectors.Length; i++)
        {
            vectors[i] = new Vector3(_figure.vertices[i].x, _figure.vertices[i].y, _figure.vertices[i].z);
        }

        Vertices = vectors;
    }

    public Vector3[] Vertices
	{
		get { return GetComponent<MeshFilter>().mesh.vertices; }
		set
		{
			if (value.Length != mesh.uv.Length)
			{
				Debug.LogError("UV's array lenght != Vertices array lenght " + value.Length + " / " + mesh.uv.Length);
				return;
			}
			
			GetComponent<MeshFilter>().mesh.vertices = value;
		}
	}
}