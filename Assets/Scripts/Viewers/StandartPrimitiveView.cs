﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StandartPrimitiveView : MonoBehaviour, IViewer
{

    private void CreateStandartPrimitives(Figures _figure)
    {
        Vector3 position = new Vector3(_figure.posX, _figure.posY);
        transform.position = position;
        transform.localScale = GetScaleValue(_figure);
    }

    private Vector3 GetScaleValue(Figures _figure)
    {
        FigureType figure = (FigureType)Enum.Parse(typeof(FigureType), _figure.type);
        switch (figure)
        {
            case FigureType.SPHERE:
                return new Vector3(_figure.radius, _figure.radius, _figure.radius);
            case FigureType.CUBE:
                return new Vector3(_figure.edgeLength, _figure.edgeLength, _figure.edgeLength);
            default:
                return Vector3.one;
        }
    }

    public void SetupViewer(Figures _figure)
    {
        CreateStandartPrimitives(_figure);
    }
}
