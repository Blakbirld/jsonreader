﻿using UnityEngine;
using UnityEngine.UI;

public class SceneController : MonoBehaviour
{
    [SerializeField] private string fileName = "SceneData";
    [SerializeField] private Text text;
    private const string prefabFolderName = "Prefabs/";

    // Use this for initialization
    void Start()
    {
        string jsonText = LoadTextFile(fileName);
        if (!string.IsNullOrEmpty(jsonText))
            ParseJson(jsonText);

    }

    private string LoadTextFile(string _fileName)
    {
        var targetFile = Resources.Load<TextAsset>(_fileName);
        return targetFile.text;
    }

    private void ParseJson(string json)
    {
        var wrapper = JsonHelpers.FromJson<Figures>(json);
        if (wrapper != null)
        {
            if (!string.IsNullOrEmpty(wrapper.levelName))
                text.text = wrapper.levelName;

            if (wrapper.figures != null)
            {
                for (int i = 0; i < wrapper.figures.Length; i++)
                {
                    GameObject obj = Instantiate(Resources.Load(prefabFolderName + wrapper.figures[i].type, typeof(GameObject))) as GameObject;
                    if (obj != null)
                        obj.GetComponent<IViewer>().SetupViewer(wrapper.figures[i]);
                }
            }
        }
    }
}
