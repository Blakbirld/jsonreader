﻿using System;
using UnityEngine;

public static class JsonHelpers
{
    public static Wrapper<T> FromJson<T>(string json)
    {
        Wrapper<T> wrapper = JsonUtility.FromJson<Wrapper<T>>(json);
       
        return wrapper;
    }

    public static string ToJson<T>(T[] array)
    {
        Wrapper<T> wrapper = new Wrapper<T>();
        wrapper.figures = array;
        return JsonUtility.ToJson(wrapper, true);
    }

    [Serializable]
    public class Wrapper<T>
    {
        public string levelName;
        public T[] figures;
    }



}
