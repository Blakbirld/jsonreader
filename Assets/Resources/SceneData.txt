{
	"levelName": "level_1",
	"figures": [{
			"type": "CUBE",
			"posX": 0.0,
			"posY": 0.0,
			"edgeLength": 2.0
		},
		{
			"type": "SPHERE",
			"posX": 5.0,
			"posY": 5.0,
			"radius": 1.5
		}, {
			"type": "PIRAMIDE",
			"vertices": [{
				"x": 0.0,
				"y": 0.0,
				"z": 0.0
			}, {
				"x": 0.0,
				"y": 1.0,
				"z": 0.0
			}, {
				"x": 0.320,
				"y": 1.0,
				"z": 0.0
			}, {
				"x": 1.0,
				"y": 0.68,
				"z": -1.0
			}]
		}
	]
}